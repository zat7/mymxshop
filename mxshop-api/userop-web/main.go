package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin/binding"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"github.com/nacos-group/nacos-sdk-go/inner/uuid"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"mxshop-api/userop-web/global"
	"mxshop-api/userop-web/initialize"
	"mxshop-api/userop-web/utils/register/consul"
	myvalidator "mxshop-api/userop-web/validator"
)

func main() {

	//1.初始化logger
	initialize.InitLogger()
	//2.初始化配置文件
	initialize.InitConfig()
	initialize.InitRealIp()
	//3.初始化routers
	Router := initialize.Routers()
	//initialize.InitMetric(Router)
	//4.初始化翻译
	if err := initialize.InitTrans("zh"); err != nil {
		panic(err)
	}
	//5.初始化srv的连接
	initialize.InitSrvConn()
	zap.S().Info("Config is", global.ServerConfig)
	viper.AutomaticEnv()
	//1.如果是本地开发环境，端口固定2.线上开发环境自动部署端口号
	//debug := viper.GetBool("MXSHOP_DEBUG")
	//if !debug {
	//	port, err := utils.GetFreePort()
	//	if err == nil {
	//		global.ServerConfig.Port = port
	//	}
	//}
	/*
		1.S()可以获取一个全局的sugar，可以让我们设置一个全局的logger
		2.日志是分级别的, debug, info, warn, error, fatal
		3.S函数和L函数很有用， 提供了一个全局的安全访问logger的途径
	*/
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("mobile", myvalidator.ValidateMobile)
		_ = v.RegisterTranslation("mobile", global.Trans, func(ut ut.Translator) error {
			return ut.Add("mobile", "{0} 非法的手机号码!", true) // see universal-translator for details
		}, func(ut ut.Translator, fe validator.FieldError) string {
			t, _ := ut.T("mobile", fe.Field())
			return t
		})
	}
	register_client := consul.NewRegistryClient(global.ServerConfig.ConsulInfo.Host, global.ServerConfig.ConsulInfo.Port)
	serviceId, _ := uuid.NewV4()
	serviceIdStr := fmt.Sprintf("%s", serviceId)
	err := register_client.Register(global.ServerConfig.Host, global.ServerConfig.Port, global.ServerConfig.Name, global.ServerConfig.Tags, serviceIdStr)
	if err != nil {
		zap.S().Panic("服务注册失败", err.Error())
	} else {
		zap.S().Info("服务注册成功")
	}

	//metricServiceId, _ := uuid.NewV4()
	//metricServiceIdStr := fmt.Sprintf("%s", metricServiceId)
	//err = register_client.Register(global.ServerConfig.Host, global.ServerConfig.Metric, global.ServerConfig.Name+"-metric", global.ServerConfig.Tags, metricServiceIdStr)
	//if err != nil {
	//	zap.S().Panic("监控服务注册失败", err.Error())
	//} else {
	//	zap.S().Info("监控服务注册成功")
	//}

	zap.S().Debugf("启动服务器, 端口: %d", global.ServerConfig.Port)
	go func() {
		if err := Router.Run(fmt.Sprintf(":%d", global.ServerConfig.Port)); err != nil {
			zap.S().Panicf("启动失败:", err.Error())
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	if err := register_client.DeRegister(serviceIdStr); err != nil {
		zap.S().Panic("注销失败", err.Error())
	} else {
		zap.S().Info("注销成功")
	}

	//if err := register_client.DeRegister(metricServiceIdStr); err != nil {
	//	zap.S().Panic("监控注销失败", err.Error())
	//} else {
	//	zap.S().Info("监控注销成功")
	//}

}

package global

import (
	ut "github.com/go-playground/universal-translator"
	"mxshop-api/order-web/config"
	"mxshop-api/order-web/proto"
)

var (
	NacosConfig        *config.NacosConfig  = &config.NacosConfig{}
	ServerConfig       *config.ServerConfig = &config.ServerConfig{}
	Trans              ut.Translator
	GoodsSrvClient     proto.GoodsClient
	OrderSrvClient     proto.OrderClient
	InventorySrcClient proto.InventoryClient
)

package router

import (
	"github.com/gin-gonic/gin"
	"mxshop-api/goods-web/api/category"
	"mxshop-api/goods-web/middlewares"
)

func InitCategoryRouter(Router *gin.RouterGroup) {
	GoodsRouter := Router.Group("categorys").Use(middlewares.Trace())

	{
		GoodsRouter.GET("", category.List)                                                   //商品列表
		GoodsRouter.POST("", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.New) //改接口需要管理员权限
		GoodsRouter.GET("/:id", category.Detail)
		GoodsRouter.DELETE("/:id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.Delete)

		GoodsRouter.PUT("/:id", middlewares.JWTAuth(), middlewares.IsAdminAuth(), category.Update)
	}
}

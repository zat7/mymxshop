package main

import (
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/inner/uuid"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/viper"
	"go.uber.org/zap"

	"mxshop-api/goods-web/global"
	"mxshop-api/goods-web/initialize"
	"mxshop-api/goods-web/utils/register/consul"
)

func main() {

	//1.初始化logger
	initialize.InitLogger()
	//2.初始化配置文件
	initialize.InitConfig()
	initialize.InitRealIp()
	//3.初始化routers
	Router := initialize.Routers()
	//initialize.InitMetric(Router)
	//4.初始化翻译
	if err := initialize.InitTrans("zh"); err != nil {
		panic(err)
	}
	//5.初始化srv的连接
	initialize.InitSrvConn()
	//6.初始化sentinel
	initialize.InitSentinel()
	viper.AutomaticEnv()
	zap.S().Info("Config is", global.ServerConfig)
	//1.如果是本地开发环境，端口固定2.线上开发环境自动部署端口号
	//debug := viper.GetBool("MXSHOP_DEBUG")
	////debug = false
	//if !debug {
	//	port, err := utils.GetFreePort()
	//	if err == nil {
	//		global.ServerConfig.Port = port
	//	}
	//
	//}
	/*
		1.S()可以获取一个全局的sugar，可以让我们设置一个全局的logger
		2.日志是分级别的, debug, info, warn, error, fatal
		3.S函数和L函数很有用， 提供了一个全局的安全访问logger的途径
	*/
	register_client := consul.NewRegistryClient(global.ServerConfig.ConsulInfo.Host, global.ServerConfig.ConsulInfo.Port)
	serviceId, _ := uuid.NewV4()
	serviceIdStr := fmt.Sprintf("%s", serviceId)
	err := register_client.Register(global.ServerConfig.Host, global.ServerConfig.Port, global.ServerConfig.Name, global.ServerConfig.Tags, serviceIdStr)
	if err != nil {
		zap.S().Panic("服务注册失败", err.Error())
	} else {
		zap.S().Info("服务注册成功")
	}

	zap.S().Debugf("启动服务器, 端口: %d", global.ServerConfig.Port)
	go func() {
		if err := Router.Run(fmt.Sprintf(":%d", global.ServerConfig.Port)); err != nil {
			zap.S().Panicf("启动失败:", err.Error())
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	if err := register_client.DeRegister(serviceIdStr); err != nil {
		zap.S().Panic("注销失败", err.Error())
	} else {
		zap.S().Info("注销成功")
	}
	
}

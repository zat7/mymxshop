package initialize

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/penglongli/gin-metrics/ginmetrics"
	"mxshop-api/goods-web/global"
)

func InitMetric(router *gin.Engine) {
	metricRouter := gin.Default()

	m := ginmetrics.GetMonitor()
	// use metric middleware without expose metric path
	m.UseWithoutExposingEndpoint(router)
	// set metric path expose to metric router
	m.SetMetricPath("/metrics")
	m.Expose(metricRouter)

	go func() {
		_ = metricRouter.Run(fmt.Sprintf(":%d", global.ServerConfig.Metric))
	}()
}

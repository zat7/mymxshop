package initialize

import (
	"fmt"
	"mxshop-api/goods-web/global"
	"net"
)

func InitRealIp() {
	conn, err := net.Dial("udp", "8.8.8.8:8")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	global.ServerConfig.Host = fmt.Sprintf("%s", localAddr.IP)
}

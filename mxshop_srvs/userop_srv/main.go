package main

import (
	"flag"
	"fmt"
	"mxshop_srvs/userop_srv/global"
	"mxshop_srvs/userop_srv/handler"
	"mxshop_srvs/userop_srv/initialize"
	"mxshop_srvs/userop_srv/proto"
	"mxshop_srvs/userop_srv/utils/register/consul"
	"net"
	"os"
	"os/signal"
	"syscall"

	uuid "github.com/satori/go.uuid"

	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"

	"go.uber.org/zap"

	"google.golang.org/grpc"
)

func main() {

	initialize.InitLogger()
	initialize.InitConfig()
	initialize.InitDB()
	initialize.InitRealIp()
	//initialize.InitMetric()
	zap.S().Info(global.ServerConfig)
	zap.S().Info("Config is", global.ServerConfig)
	flag.Parse()

	//server := grpc.NewServer(grpc.UnaryInterceptor(initialize.UnaryPrometheusInterceptor))
	server := grpc.NewServer()
	proto.RegisterAddressServer(server, &handler.UserOpServer{})
	proto.RegisterMessageServer(server, &handler.UserOpServer{})
	proto.RegisterUserFavServer(server, &handler.UserOpServer{})
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", global.ServerConfig.Host, global.ServerConfig.Port))
	if err != nil {
		panic("failed to listen" + err.Error())
	}
	//注册服务健康检查
	grpc_health_v1.RegisterHealthServer(server, health.NewServer())
	go func() {
		err = server.Serve(lis)
		if err != nil {
			panic("failed to start rpc" + err.Error())
		}
	}()

	register_client := consul.NewRegistryClient(global.ServerConfig.ConsulInfo.Host, global.ServerConfig.ConsulInfo.Port)
	serviceID := fmt.Sprintf("%s", uuid.NewV4())
	err = register_client.Register(global.ServerConfig.Host, global.ServerConfig.Port, global.ServerConfig.Name, global.ServerConfig.Tags, serviceID)
	if err != nil {
		zap.S().Panic("服务注册失败", err.Error())
	} else {
		zap.S().Info("服务注册成功")
	}
	/*
		1.S()可以获取一个全局的sugar，可以让我们设置一个全局的logger
		2.日志是分级别的, debug, info, warn, error, fatal
		3.S函数和L函数很有用， 提供了一个全局的安全访问logger的途径
	*/
	zap.S().Debugf("启动服务器, 端口: %d", global.ServerConfig.Port)

	//接收终止信号
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	if err := register_client.DeRegister(serviceID); err != nil {
		zap.S().Panic("注销失败", err.Error())
	} else {
		zap.S().Info("注销成功")
	}
}

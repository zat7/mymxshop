package main

import (
	"fmt"
	"github.com/apache/rocketmq-client-go/v2"
	"github.com/apache/rocketmq-client-go/v2/consumer"
)

func main() {
	c, err := rocketmq.NewPushConsumer(
		consumer.WithNameServer([]string{fmt.Sprintf("http://%s:%d", "", 9876)}),
		consumer.WithGroupName("mxshop-order"),
	)
	if err != nil {
		panic(err)
	}
	fmt.Println(c)
}

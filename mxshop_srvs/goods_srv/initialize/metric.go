package initialize

import (
	"context"
	"flag"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"mxshop_srvs/goods_srv/global"
	"net/http"
	"strconv"
	"time"
)

const (
	serverNamespace = "GOODS_SRV"
)

var (
	normDomain = flag.Float64("normal.domain", 0.0002, "The domain for the normal distribution.")
	normMean   = flag.Float64("normal.mean", 0.00001, "The mean for the normal distribution.")
)

var (
	RpcDurations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:  serverNamespace,
			Name:       "rpc_durations_seconds",
			Help:       "RPC latency distributions.这个metric的帮助信息,metric的项目作用说明",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"service"},
	)

	MetricServerReqCodeTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: serverNamespace,
		Subsystem: "requests",
		Name:      "mxshop_code_total",
		Help:      "rpc server requests code count",
	}, []string{"code"})
)

func UnaryPrometheusInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	startTime := time.Now()
	resp, err = handler(ctx, req)
	RpcDurations.WithLabelValues(info.FullMethod).Observe(float64(time.Since(startTime) / time.Millisecond))
	MetricServerReqCodeTotal.WithLabelValues(strconv.Itoa(int(status.Code(err)))).Inc()
	return nil, nil
}
func InitMetric() {
	prometheus.MustRegister(MetricServerReqCodeTotal, RpcDurations)
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(fmt.Sprintf(":%d", global.ServerConfig.Metric), nil)
}

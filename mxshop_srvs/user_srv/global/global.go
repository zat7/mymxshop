package global

import (
	"gorm.io/gorm"
	"mxshop_srvs/user_srv/config"
)

var (
	DB           *gorm.DB
	ServerConfig config.ServerConfig
	NacosConfig  config.NacosConfig
)

//func init() {
//	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
//	dsn := "root:root@tcp(192.168.137.83:3306)/mxshop_user_srv?charset=utf8mb4&parseTime=True&loc=Local"
//	newLogger := logger.New(
//		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
//		logger.Config{
//			SlowThreshold:             time.Second, // 慢 SQL 阈值
//			LogLevel:                  logger.Info, // 日志级别
//			IgnoreRecordNotFoundError: true,        // 忽略ErrRecordNotFound（记录未找到）错误
//			Colorful:                  true,        // 禁用彩色打印
//		},
//	)
//
//	// 全局模式
//	var err error
//	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
//		Logger: newLogger,
//	})
//	if err != nil {
//
//		panic(err)
//	}
//}
